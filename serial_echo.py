# Serial echo test from Python on a Raspberry Pi or laptop to
# an Arduino
#
# Cases:
#
# 1: Raspberry Pi GPIO
# 2: Raspberry Pi USB
# 3: Mac/Linux USB
# 4: Windows
#
# Note: You will probably need to edit the port for a laptop
#       based on what you see under port in your Arduino IDE.

case = 3
if case == 1:
    portname = '/dev/ttyAMA0'
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)

elif case == 2:
    #USB
    portname = '/dev/ttyACM0'

elif case == 3:
    portname = '/dev/cu.usbmodem1421'#<-- edit this as needed

elif case == 4:
    portname = 'COM3'#<-- edit this as needed

    
import serial
ser = serial.Serial(portname, 115200, timeout=1)
#ser.open()

import serial_utils

if case > 1:
    debug_line = serial_utils.Read_Line(ser)
    line_str = ''.join(debug_line)

from matplotlib.pyplot import *
from numpy import *
import numpy, time


import time, copy, os

ser.flushInput()
ser.flushOutput()

N = 175

nvect = zeros(N,dtype=int)
v1 = arange(0,N)
v_echo = zeros_like(nvect)
msb_list = []
lsb_list = []

serial_utils.WriteByte(ser, 2)#start new test
check_byte = serial_utils.Read_Two_Bytes(ser)

t0 = time.time()

for i in range(N):
    serial_utils.WriteByte(ser, 1)#new n and voltage are coming
    serial_utils.WriteInt(ser, i)
    serial_utils.WriteInt(ser, v1[i])

    nvect[i] = serial_utils.Read_Two_Bytes(ser)
    #v_echo[i] = serial_utils.Read_Two_Bytes(ser)
    v_echo[i] = serial_utils.Read_Two_Bytes_Twos_Comp(ser)

    #msb = serial_utils.Read_Byte(ser)
    #lsb = serial_utils.Read_Byte(ser)
    #msb = ser.read(1)
    #lsb = ser.read(1)
    
    nl_check = serial_utils.Read_Byte(ser)
    assert nl_check == 10, "newline problem"

    #lsb_list.append(lsb)
    #msb_list.append(msb)
    

t1 = time.time()
print('Total time = %0.4g' % (t1-t0))
dt_ave = (t1-t0)/N
print('average dt = %0.4g' % (dt_ave))

time.sleep(0.1)
serial_utils.WriteByte(ser, 3)#stop test
time.sleep(0.1)
serial_utils.WriteByte(ser, 3)#stop test

serial_utils.Close_Serial(ser)

if case > 1:
    print(line_str)


#msb_int = [ord(item) for item in msb_list]
#lsb_int = [ord(item) for item in lsb_list]

neg_inds = where(v_echo < 0)[0]
v_echo[neg_inds] += 2**16

test_vect = v1**2-v_echo

if test_vect.any():
    print('some failures:')
    print(test_vect)
else:
    print('no failures')

figure(1)
clf()
plot(nvect, v1**2, nvect, v_echo, 'ro')


t = dt_ave*nvect

data = array([t, v1, v_echo]).T


def save_data(filename, datain):
    #provide filename extension if there isn't one
    fno, ext = os.path.splitext(filename)
    if not ext:
        ext = '.csv'
    filename = fno + ext

    labels = ['#t','v','theta']

    data_str = datain.astype(str)
    data_out = numpy.append([labels],data_str, axis=0)

    savetxt(filename, data_out, delimiter=',')
    

show()
