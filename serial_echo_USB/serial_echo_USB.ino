#define isrPin 5
#define sendPin 8
#define receivePin 7
#define triggerPin 12

const byte mask = B11111000;
int prescale = 1;

int n;
int nIn;
int nISR;
int v1;
int v_out;

int inByte;
int fresh;
bool send_ser;

void setup()
{
  Serial.begin(115200);

  Serial.print("serial echo test USB 05/01/17");
  Serial.print("\n");

  //Serial1.begin(115200);

  send_ser = false;


  pinMode(isrPin, OUTPUT);
  pinMode(sendPin, OUTPUT);
  pinMode(receivePin, OUTPUT);
  pinMode(triggerPin, OUTPUT);

  digitalWrite(isrPin, LOW);
  digitalWrite(receivePin, LOW);
  digitalWrite(triggerPin, LOW);
  digitalWrite(sendPin, LOW);

  // turn on pullup resistors
  //digitalWrite(encoderPinA, HIGH);
  //digitalWrite(encoderPinB, HIGH);

  //=======================================================
  // set up the Timer2 interrupt to trigger at 100Hz
  //=======================================================
  cli();          // disable global interrupts
  //OCR0A = 124;
  TCCR2A = 0;// set entire TCCR2A register to 0
  TCCR2B = 0;// same for TCCR2B
  TCNT2  = 0;//initialize counter value to 0
  // set compare match register for 8khz increments
  OCR2A = 124;// = (16*10^6) / (8000*8) - 1 (must be <256)
  // turn on CTC mode
  TCCR2A |= (1 << WGM21);
  // Set CS21 bit for 8 prescaler
  //  TCCR2B |= (1 << CS21);
  bitSet(TCCR2B, CS22);
  bitSet(TCCR2B, CS21);
  bitSet(TCCR2B, CS20);
  //!//bitClear(TCCR2B, CS20);
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);
  sei();// re-enable global interrupts
  //=======================================================
}

unsigned char getsecondbyte(int input){
    unsigned char output;
    output = (unsigned char)(input >> 8);
    return output;
}

 

int reassemblebytes(unsigned char msb, unsigned char lsb){
    int output;
    output = (int)(msb << 8);
    output += lsb;
    return output;
}

int readtwobytes(void){
    unsigned char msb, lsb;
    int output;
    int iter = 0;
    while (Serial.available() <2){
      iter++;
      if (iter > 1e5){
	break;
      }
    }
    msb = Serial.read();
    lsb = Serial.read();
    output = reassemblebytes(msb, lsb);
    return output;
}

void SendTwoByteInt(int intin){
    unsigned char lsb, msb;
    lsb = (unsigned char)intin;
    msb = getsecondbyte(intin);
    Serial.write(msb);
    Serial.write(lsb);
}



void loop()
{
  if (Serial.available() > 0) {
    digitalWrite(receivePin, HIGH);
    inByte = Serial.read();
    if (inByte == 1){
      //main control case
      //send_ser = true;
      nIn = readtwobytes();
      v1 = readtwobytes();
    }
    else if (inByte == 2){
      //start new test
      send_ser = true;
      nISR = -1;
      delay(5);
      SendTwoByteInt(2);
    }
    else if (inByte == 3){
      send_ser = false;
      v1 = 0;
    }
    digitalWrite(receivePin, LOW);
  }
  
  if (fresh > 0){
    fresh = 0;
    if (send_ser){
      digitalWrite(sendPin, HIGH);
      //send_ser = false;
      //SendTwoByteInt(nISR);
      SendTwoByteInt(nIn);
      SendTwoByteInt(v_out);
      //SendTwoByteInt(v1);
      Serial.write(10);
      digitalWrite(sendPin, LOW);
    }
    if (nISR > 500){
      send_ser = false;
      v1 = 0;
    }
  }
}


ISR(TIMER2_COMPA_vect)
{     
  digitalWrite(isrPin, HIGH);
  nISR++;
  //analogWrite(pwmA, v1);
  v_out = v1*v1;
  fresh = 1;
  digitalWrite(isrPin, LOW);
}
